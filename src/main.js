import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios' //cliente HTTP
import { library } from '@fortawesome/fontawesome-svg-core'
import { faFacebook, faTwitter,faInstagram } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import './assets/main.css'
import './assets/fonts.css'
import './assets/medias.css'

//BOTON MENU
//Este codigo  esta escuchando el evento DOMContentLoaded,
// lo que significa que se ejecutara cuando la pagina haya terminado de cargar
window.addEventListener("DOMContentLoaded",(event)=>{
    /*Obtnemos los elementos */
    const menubtn=document.getElementById("menu");
    const nav=document.querySelector("header nav");
    const body =document.querySelector("body");
    

    menubtn.addEventListener("click",(event)=>{        
        /*
        El método toggle() agrega la clase "salir" si el elemento no la tiene, 
        y la elimina si el elemento ya tiene la clase "salir". Por lo tanto, 
        cada vez que se hace clic en el botón del menú, la clase "salir" 
        se agrega o se quita del elemento menubtn.
         */
        menubtn.classList.toggle("salir");
        nav.classList.toggle("visible");
        body.classList.toggle("no-scroll");      
    })

   
    

    
});

//iconos redes sociales
library.add(faFacebook, faTwitter,faInstagram)


const app = createApp(App)

app.component('font-awesome-icon', FontAwesomeIcon)
axios.defaults.baseURL='http://localhost:8081/api/'

app.use(router)

app.mount('#app')

