import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/Marvel/:type",
      name: "marvel",
      component: () => import("../views/MarvelDCView.vue"),
    },
    {
      path: "/DC/:type",
      name: "dc",
      component: () => import("../views/MarvelDCView.vue"),
    },
  ],
});

export default router;
